FROM nginx
MAINTAINER awayisblue, awayisblue@qq.com
COPY index.dist.html /usr/share/nginx/html/index.html
COPY dist /usr/share/nginx/html/dist
# 调试时，可以通过volume-from 挂载nginx目录到其它容器
VOLUME /usr/share/nginx/html
VOLUME /etc/nginx

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
