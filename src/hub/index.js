let hub = {}
export default {
  setStore (store) {
    hub['store'] = store
  },
  getStore () {
    return hub['store']
  }
}
