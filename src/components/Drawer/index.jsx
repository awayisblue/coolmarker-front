import React, { Component } from 'react'
import CollapseDrawer from '../common/CollapseDrawer'
import style from './styles.less'
class Drawer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: []
    }
    this.open = this.open.bind(this)
    this.close = this.close.bind(this)
    this.keep = this.keep.bind(this)
  }
  open () {
    let copy = JSON.parse(JSON.stringify(this.state.open))
    let lastIdx = copy.length - 1
    if (lastIdx > -1 && !copy[copy.length - 1]) {
      copy[copy.length - 1] = true
    } else if (copy.length < 4) {
      copy.push(true)
    }
    this.setState({
      open: copy
    })
  }
  close (index) {
    let copy = JSON.parse(JSON.stringify(this.state.open))
    if (!copy[index]) return
    copy = copy.slice(0, index)
    this.setState({
      open: copy
    })
  }
  keep () {
    this.setState({
      open: [false, false, true, false]
    })
  }
  render () {
    let open = <div className={style.open} onClick={this.open}>
      open
    </div>
    let keep = <div className={style.keep} onClick={this.keep}>
      keep
    </div>
    return (
      <div className={style.base}>
        <CollapseDrawer drawersOpen={this.state.open}>
          <div onClick={() => this.close(0)}>
            drawer1
          </div>
          <div onClick={() => this.close(1)}>
            drawer2
          </div>
          <div onClick={() => this.close(2)}>
            drawer3
          </div>
          <div onClick={() => this.close(3)}>
            drawer4
          </div>
        </CollapseDrawer>
        {open}
        {keep}
      </div>
    )
  }
}
export default Drawer
