import React, { Component } from 'react'
import style from './styles.less'
import {
  CSSTransition,
  TransitionGroup,
} from 'react-transition-group'
/**
 * docs
 */
class CollapseDrawer extends Component {
  constructor (props) {
    super(props)
    let children = this.props.children
    if (!children) this.drawers = []
    if (children.length) {
      this.drawers = children
    } else {
      this.drawers = [children]
    }
  }
  render () {
    let drawersOpen = this.props.drawersOpen
    let drawersRender = this.drawers.map((drawer, index) => {
      if (drawersOpen[index]) {
        return <CSSTransition timeout={300} key={index} classNames={'drawer'}>
          {this.drawers[index]}
        </CSSTransition>
      }
      return null
    })
    console.log(drawersRender)
    return (
      <TransitionGroup className={style.base}>
        {drawersRender}
      </TransitionGroup>
    )
  }
}
export default CollapseDrawer
