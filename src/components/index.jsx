import React, { Component } from 'react'
import Drawer from './Drawer'
class Root extends Component {
  render () {
    return (
      <div>
        <Drawer />
      </div>
    )
  }
}
export default Root
